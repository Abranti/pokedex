package com.example.pokedex.utils;

public class Constants {
    public static final int TEMPO_SPLASH_SCREEN = 2000;
    public static final String API_BASE_URL = "http://pokeapi.co/api/v2/";
    public static final String URL_TIPOS = "type/";
    public static final String NOME = "Nome: \n";
    public static final String PESO = "\nPeso: \n";
    public static final String ALTURA = "\nAltura: \n";
    public static final String HABILIDADES = "Habilidades: \n\n";
}
