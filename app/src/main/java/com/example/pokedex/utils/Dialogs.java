package com.example.pokedex.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.pokedex.R;

public class Dialogs {

    /**
     * Mostra dialog de erro de conexao
     * @param activity
     */
    public static void mostrarDialogSemConexaoRecarregar(final Activity activity) {
        final Dialog erroConectividadeDialog = new Dialog(activity);
        erroConectividadeDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        erroConectividadeDialog.setContentView(activity.getLayoutInflater().inflate(R.layout.layout_dialog_erro_conectividade
                , null));
        ((Button) erroConectividadeDialog.findViewById(R.id.botao_dialog))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        erroConectividadeDialog.dismiss();
                        activity.finish();
                        activity.startActivity(activity.getIntent());
                    }
                });
        mostrarProgressDialog(activity, false);
        erroConectividadeDialog.setCancelable(false);
        if(!erroConectividadeDialog.isShowing())  {
            erroConectividadeDialog.show();
        }
    }

    /**
     * Mostra dialog de ero generico na aplicacao
     * @param activity
     */
    public static void mostrarDialogErro(final Activity activity) {
        final Dialog erroConectividadeDialog = new Dialog(activity);
        erroConectividadeDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        erroConectividadeDialog.setContentView(activity.getLayoutInflater().inflate(R.layout.layout_dialog_erro_conectividade_inicial
                , null));
        ((Button) erroConectividadeDialog.findViewById(R.id.botao_dialog))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        erroConectividadeDialog.dismiss();
                        activity.finish();
                    }
                });
        mostrarProgressDialog(activity, false);
        erroConectividadeDialog.setCancelable(false);
        erroConectividadeDialog.show();
    }

    /**
     * Mostra dialog de erro quando nao houver pokemon para o tipo solicitado
     * @param activity
     */
    public static void mostrarDialogErroSemPokemon(final Activity activity) {
        final Dialog erroConectividadeDialog = new Dialog(activity);
        erroConectividadeDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        erroConectividadeDialog.setContentView(activity.getLayoutInflater().inflate(R.layout.layout_dialog_erro_tipo_sem_pokemon
                , null));
        ((Button) erroConectividadeDialog.findViewById(R.id.botao_dialog))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        erroConectividadeDialog.dismiss();
                        activity.finish();
                    }
                });
        mostrarProgressDialog(activity, false);
        erroConectividadeDialog.setCancelable(false);
        erroConectividadeDialog.show();
    }

    /**
     * Mostra dialog de loading
     * @param activity
     * @param show
     */
    public static void mostrarProgressDialog(final Activity activity, Boolean show) {
        ProgressBar progressBar = activity.findViewById(R.id.progressBar);
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
