package com.example.pokedex.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Pokemon implements Serializable, Comparator<Pokemon> {
    private String nome;
    private String fotoUrl;
    private String url;
    private Integer altura;
    private Integer peso;
    private ArrayList<String> habilidades = new ArrayList<>();

    public Pokemon() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Float getAltura() {
        return (float) altura;
    }

    public void setAltura(Integer altura) {
        this.altura = altura;
    }

    public Float getPeso() {
        return (float) peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public ArrayList<String> getHabilidades() {
        return habilidades;
    }

    public void setHabilidades(ArrayList<String> habilidades) {
        this.habilidades = habilidades;
    }

    public Pokemon(JSONObject jsonObject) {
        ArrayList<String> listaHabilidades = new ArrayList<>();
        try {
            this.nome = jsonObject.getString("name");
            this.fotoUrl = jsonObject.getJSONObject("sprites").getString("front_default");
            this.altura = jsonObject.getInt("height");
            this.peso = jsonObject.getInt("weight");

            for(int i = 0; i < jsonObject.getJSONArray("abilities").length(); i++) {
                JSONObject habilidade = (JSONObject) jsonObject.getJSONArray("abilities").get(i);
                listaHabilidades.add(habilidade.getJSONObject("ability").getString("name"));
            }

            this.habilidades = listaHabilidades;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int compare(Pokemon o1, Pokemon o2) {
        return o1.getNome().compareTo(o2.getNome());
    }
}
