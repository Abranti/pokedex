package com.example.pokedex.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Comparator;

public class TipoPokemon implements Serializable, Comparator<TipoPokemon> {

    private String nome;
    private String url;

    public TipoPokemon() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String name) {
        this.nome = nome;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public TipoPokemon(JSONObject jsonObject) {
        try {
            this.nome = jsonObject.getString("name");
            this.url = jsonObject.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int compare(TipoPokemon o1, TipoPokemon o2) {
        return o1.getNome().compareTo(o2.getNome());
    }
}
