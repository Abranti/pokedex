package com.example.pokedex.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.pokedex.R;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.view.DetalhesPokemonActivity;

import java.util.ArrayList;

public class RecyclerViewListAdapterPokemon extends RecyclerView.Adapter<RecyclerViewListAdapterPokemon.MyViewHolder> {

    ArrayList<Pokemon> listaPokemons;
    Activity activity;

    public RecyclerViewListAdapterPokemon(ArrayList<Pokemon> listaPokemons, Activity activity){
        this.listaPokemons = listaPokemons;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        CardView cardView;
        public MyViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.nomePokemon);
            cardView = itemView.findViewById(R.id.card_view);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Intent intent = new Intent(activity, DetalhesPokemonActivity.class);
                    intent.putExtra("pokemon", listaPokemons.get(getAdapterPosition()));
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_card_pokemon_lista, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.textView.setText(listaPokemons.get(position).getNome().substring(0, 1).toUpperCase() + listaPokemons.get(position).getNome().substring(1));
    }

    @Override
    public int getItemCount() {
        return listaPokemons.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
