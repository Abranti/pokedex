package com.example.pokedex.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pokedex.R;
import com.example.pokedex.model.TipoPokemon;
import com.example.pokedex.view.TipoActivity;

import java.util.ArrayList;

public class RecyclerViewListAdapterTipo extends RecyclerView.Adapter<RecyclerViewListAdapterTipo.MyViewHolder> {

    ArrayList<TipoPokemon> listaTiposPokemon;
    Activity activity;

    public RecyclerViewListAdapterTipo(ArrayList<TipoPokemon> listaTiposPokemon, Activity activity){
        this.listaTiposPokemon = listaTiposPokemon;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        CardView cardView;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imagemTipo);
            textView = itemView.findViewById(R.id.nomeTipo);
            cardView = itemView.findViewById(R.id.card_view);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Intent intent = new Intent(activity, TipoActivity.class);
                    intent.putExtra("tipoPokemon", listaTiposPokemon.get(getAdapterPosition()));
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_card_tipo, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        int idTipo = activity.getResources().getIdentifier(listaTiposPokemon.get(position).getNome(), "drawable", activity.getPackageName());
        holder.imageView.setBackgroundResource(idTipo);
        holder.textView.setText(listaTiposPokemon.get(position).getNome().substring(0, 1).toUpperCase() + listaTiposPokemon.get(position).getNome().substring(1));
    }

    @Override
    public int getItemCount() {
        return listaTiposPokemon.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
