package com.example.pokedex.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.example.pokedex.adapters.RecyclerViewListAdapterTipo;
import com.example.pokedex.R;
import com.example.pokedex.utils.Connections;
import com.example.pokedex.utils.Dialogs;
import com.example.pokedex.controller.ApiCallController;
import com.example.pokedex.model.TipoPokemon;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;

public class TiposPokemonsActivity extends AppCompatActivity {
    ApiCallController apiCallController = new ApiCallController();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipos_pokemons);

        try {
            this.carregarTiposPokemons();
        } catch (JSONException e) {
            e.printStackTrace();
            Dialogs.mostrarDialogErro(this);
        }
    }

    /**
     * Inicia processo de carga dos tipos de pokemons, verificando se existe conexao com a internet
     * @throws JSONException
     */
    private void carregarTiposPokemons() throws JSONException {
        if(Connections.isInternetConnection(this)){
            Dialogs.mostrarProgressDialog(this, true);
            apiCallController.getTiposPokemons(this);
        } else {
            Dialogs.mostrarDialogSemConexaoRecarregar(this);
        }
    }

    /**
     * Preenche os tipos de pokemon na tela juntamente com seus icones
     * @param listaTipoPokemons
     */
    public void preencherTiposPokemons(ArrayList<TipoPokemon> listaTipoPokemons) {
        TipoPokemon tipoPokemon = new TipoPokemon();
        Collections.sort(listaTipoPokemons, tipoPokemon);
        RecyclerViewListAdapterTipo recyclerViewListAdapter = new RecyclerViewListAdapterTipo(listaTipoPokemons, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewListAdapter);
        Dialogs.mostrarProgressDialog(this, false);
    }
}
