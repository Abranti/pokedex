package com.example.pokedex.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pokedex.R;
import com.example.pokedex.utils.Connections;
import com.example.pokedex.utils.Constants;
import com.example.pokedex.utils.Dialogs;
import com.example.pokedex.controller.ApiCallController;
import com.example.pokedex.controller.ShareController;
import com.example.pokedex.model.Pokemon;

import org.json.JSONException;

import java.io.IOException;

public class DetalhesPokemonActivity extends AppCompatActivity {
    ApiCallController apiCallController = new ApiCallController();
    ShareController shareController = new ShareController();
    Pokemon pokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_pokemon);

        Button botaoCompartilhar = findViewById(R.id.botao_compartilhar);
        botaoCompartilhar.setOnClickListener(compartilharOnClickListener);

        try {
            Intent intent = getIntent();
            if (intent.getSerializableExtra("pokemon") != null) {
                this.carregarPokemon((Pokemon) intent.getSerializableExtra("pokemon"));
            } else {
                Dialogs.mostrarDialogErro(this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Dialogs.mostrarDialogErro(this);
        }
    }

    /**
     * Inicia processo de carga dos pokemons, verificando se existe acesso a internet
     * @param pokemon
     * @throws JSONException
     */
    private void carregarPokemon(Pokemon pokemon) throws JSONException {
        if(Connections.isInternetConnection(this)){
            try {
                Dialogs.mostrarProgressDialog(this, true);
                apiCallController.getPokemon(this, pokemon.getUrl());
            } catch (IOException e) {
                e.printStackTrace();
                Dialogs.mostrarDialogErro(this);
            }
        } else {
            Dialogs.mostrarDialogSemConexaoRecarregar(this);
        }
    }

    /**
     * Preenche o pokemon na tela utilizando as views
     * @param pokemon
     * @param bitmap
     */
    public void preencherPokemon(Pokemon pokemon, Bitmap bitmap) {
        this.pokemon = pokemon;
        ImageView imagemPokemon = findViewById(R.id.imagemPokemon);
        TextView nomePokemon = findViewById(R.id.nomePokemon);
        TextView alturaPokemon = findViewById(R.id.alturaPokemon);
        TextView pesoPokemon = findViewById(R.id.pesoPokemon);
        TextView habilidadesPokemon = findViewById(R.id.habilidadesPokemon);

        if(bitmap != null) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
            imagemPokemon.setBackground(bitmapDrawable);
        } else {
            imagemPokemon.setBackground(ContextCompat.getDrawable(this, R.drawable.miss));
        }

        nomePokemon.setText(Constants.NOME + pokemon.getNome().substring(0, 1).toUpperCase() + pokemon.getNome().substring(1));
        pesoPokemon.setText(Constants.PESO + (pokemon.getPeso() / 10) + " Kg");
        alturaPokemon.setText(Constants.ALTURA + (pokemon.getAltura() / 10) + " m");

        String habilidades = Constants.HABILIDADES;
        for(String habilidade: pokemon.getHabilidades()) {
            habilidades = habilidades + "- " + habilidade.substring(0, 1).toUpperCase() + habilidade.substring(1) + "\n\n";
        }
        habilidadesPokemon.setText(habilidades);
        Dialogs.mostrarProgressDialog(this, false);
    }

    /**
     * Inicia o processo de compartilhamento do pokemon utilizando os dados do mesmo
     */
    private void compartilharPokemon() {
        String pokemonInfo = Constants.NOME + pokemon.getNome().substring(0, 1).toUpperCase() + pokemon.getNome().substring(1) + "\n" +
                             Constants.ALTURA + (pokemon.getAltura() / 10) + " m\n" +
                             Constants.PESO + (pokemon.getPeso() / 10) + " Kg\n\n" +
                             Constants.HABILIDADES;
        for(String habilidade: pokemon.getHabilidades()) {
            pokemonInfo = pokemonInfo + "- " + habilidade.substring(0, 1).toUpperCase() + habilidade.substring(1) + "\n\n";
        }
        shareController.externalShare(pokemonInfo, this);
    }

    /**
     * Evento de click no compartilhamento
     */
    private View.OnClickListener compartilharOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            compartilharPokemon();
        }
    };
}
