package com.example.pokedex.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.pokedex.utils.Constants;
import com.example.pokedex.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mostrarTelaTipos();
    }

    /**
     * Redireciona para atela inicial após a SplashScreen
     */
    private void mostrarTelaTipos() {

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this,
                        TiposPokemonsActivity.class);
                startActivity(intent);
                finish();
            }
        }, Constants.TEMPO_SPLASH_SCREEN);
    }
}
