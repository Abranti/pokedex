package com.example.pokedex.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pokedex.adapters.RecyclerViewListAdapterPokemon;
import com.example.pokedex.R;
import com.example.pokedex.utils.Connections;
import com.example.pokedex.utils.Dialogs;
import com.example.pokedex.controller.ApiCallController;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.model.TipoPokemon;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class TipoActivity extends AppCompatActivity {
    ApiCallController apiCallController = new ApiCallController();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo);

        try {
            Intent intent = getIntent();
            if (intent.getSerializableExtra("tipoPokemon") != null) {
                this.carregarPokemons((TipoPokemon) intent.getSerializableExtra("tipoPokemon"));
            } else {
                Dialogs.mostrarDialogErro(this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Dialogs.mostrarDialogErro(this);
        }
    }

    /**
     * Inicia o processo de carga dos pokemons pelo seu tipo, verificnado se existe conexao com a internet
     * @param tipoPokemon
     * @throws JSONException
     */
    private void carregarPokemons(TipoPokemon tipoPokemon) throws JSONException {
        if(Connections.isInternetConnection(this)){
            Dialogs.mostrarProgressDialog(this, true);
            try {
                apiCallController.getPokemonsPorTipo(this, tipoPokemon.getUrl());
            } catch (IOException e) {
                e.printStackTrace();
                Dialogs.mostrarDialogErro(this);
            }
        } else {
            Dialogs.mostrarDialogSemConexaoRecarregar(this);
        }
    }

    /**
     * Insere o nome dos pokemons do tipo na tela, verificando se existe pokemons deste tipo
     * @param listaPokemons
     */
    public void preencherPokemons(ArrayList<Pokemon> listaPokemons) {
        if(listaPokemons.size() != 0) {
            Pokemon pokemon = new Pokemon();
            Collections.sort(listaPokemons, pokemon);
            RecyclerViewListAdapterPokemon recyclerViewListAdapterPokemon = new RecyclerViewListAdapterPokemon(listaPokemons, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recyclerView = findViewById(R.id.recycler_view);
            recyclerView.setHasFixedSize(true);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(recyclerViewListAdapterPokemon);
            Dialogs.mostrarProgressDialog(this, false);
        } else {
            Dialogs.mostrarDialogErroSemPokemon(this);
        }
    }
}
