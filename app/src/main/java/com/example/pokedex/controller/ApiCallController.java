package com.example.pokedex.controller;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.example.pokedex.utils.Constants;
import com.example.pokedex.utils.Dialogs;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.model.TipoPokemon;
import com.example.pokedex.view.DetalhesPokemonActivity;
import com.example.pokedex.view.TipoActivity;
import com.example.pokedex.view.TiposPokemonsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

public class ApiCallController {

    /**
     * Chama a API buscando todos os tipos de pokemons existentes na mesma
     * @param activity
     */
    public void getTiposPokemons(final Activity activity) {
        final ArrayList<TipoPokemon> listaTipoPokemon = new ArrayList<TipoPokemon>();

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(Constants.API_BASE_URL + Constants.URL_TIPOS, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                JSONArray jsonArray = new JSONArray();
                try {
                    jsonArray = response.getJSONArray("results");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Dialogs.mostrarDialogErro(activity);
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = null;
                    try {
                        object = jsonArray.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Dialogs.mostrarDialogErro(activity);
                    }
                    TipoPokemon tipoPokemon = new TipoPokemon(object);
                    listaTipoPokemon.add(tipoPokemon);
                }
                ((TiposPokemonsActivity) activity).preencherTiposPokemons(listaTipoPokemon);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
                Dialogs.mostrarDialogErro(activity);
            }
        });
    }

    /**
     * Chama a API buscnado todos os pokemons de um tipo
     * @param activity
     * @param urlTipoPokemon
     * @throws IOException
     */
    public void getPokemonsPorTipo(final Activity activity, String urlTipoPokemon) throws IOException {
        final ArrayList<Pokemon> listaTipoPokemon = new ArrayList<Pokemon>();

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(urlTipoPokemon, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                JSONArray jsonArray = null;
                Pokemon pokemon;
                try {
                    jsonArray = response.getJSONArray("pokemon");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Dialogs.mostrarDialogErro(activity);
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = null;
                    pokemon = new Pokemon();
                    try {
                        object = jsonArray.getJSONObject(i);
                        pokemon.setNome(object.getJSONObject("pokemon").getString("name"));
                        pokemon.setUrl(object.getJSONObject("pokemon").getString("url"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Dialogs.mostrarDialogErro(activity);
                    }
                    listaTipoPokemon.add(pokemon);
                }
                ((TipoActivity) activity).preencherPokemons(listaTipoPokemon);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
                Dialogs.mostrarDialogErro(activity);
            }
        });
    }

    /**
     * Chama a API buscando um pokemon
     * @param activity
     * @param urlPokemon
     * @throws IOException
     */
    public void getPokemon(final Activity activity, String urlPokemon) throws IOException {
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(urlPokemon, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Pokemon pokemon = new Pokemon(response);
                new CarregarImagemPokemonTask(activity, pokemon).execute(pokemon.getFotoUrl());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
                Dialogs.mostrarDialogErro(activity);
            }
        });
    }

    private class CarregarImagemPokemonTask extends AsyncTask<String, Void, Bitmap> {

        Activity activity;
        Pokemon pokemon;

        public CarregarImagemPokemonTask(Activity activity, Pokemon pokemon) {
            this.activity = activity;
            this.pokemon = pokemon;
        }

        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Bitmap bitmap) {
            ((DetalhesPokemonActivity) activity).preencherPokemon(pokemon, bitmap);
        }
    }
}

