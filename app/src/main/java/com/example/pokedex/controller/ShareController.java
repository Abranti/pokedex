package com.example.pokedex.controller;

import android.content.Context;
import android.content.Intent;

import com.example.pokedex.R;

public class ShareController {

    /**
     * Reliza o compartilhamento do Popkemon
     * @param pokemonInfo
     * @param context
     */
    public void externalShare(String pokemonInfo, Context context) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, pokemonInfo);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.compartilharPokemon)));
    }
}
