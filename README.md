# Pokédex

Projeto que visa a criação de uma aplicação Android nativa com a utilização de uma API Rest, buscando exercitar meus conhecimentos em desenvolvimento Mobile Android, juntamente ao tema de Pokémon

## Objetivo

A aplicação tem por objetivo mostrar dados de Pokémons, separando os pelos seus tipos

## Aparência

Para a criação dos layouts, foi feita uma pesquisa com base nos jogos e desenhos afim de encontrar referência visuais para a criação dos layouts, por exemplo, a fonte utilizada na aplicação é a mesma fonte utilizada nos jogos de Pokémon. Dessa forma os usuários podem se identificar com conceitos já utilizados anteriormente na franquia 

## Bibliotecas Utilizadas

### Cardview

A biblioteca [Cardiview](https://developer.android.com/guide/topics/ui/layout/cardview) foi escolhida para este projeto afim de que o mesmo pudesse utilizar os conceitos de Material Design em suas views 

### Android-Async-Http

A biblioteca [Android-Async-Http](http://loopj.com/android-async-http) foi escolhida para este projeto afim de tornar possível de maneira simplista as chamadas assíncronas da API

### RecyclerView 

A biblioteca [RecyclerView](https://developer.android.com/guide/topics/ui/layout/recyclerview) foi escolhida para este projeto por se tratar de um widget mais avançado e eficiente, quando comparado aos seus antecessores, e que apresenta diversas simplificações para suportar animações e diferentes disposições de elementos

## API

O [PokéAPI](https://pokeapi.co) fornece integração com um extenso banco de dados Pokémon. A versão 2 oferece recursos como locais que podem ser visitados nos jogos, habilidades de Pokémon e cadeias de evolução

## Ambiente

Para a criação desta aplicação foi utilizado as seguintes configurações de ambiente:

- Sistema Operacional: Windows
- Android Studio 3.3
- Java 1.8
- Teste da Aplicação: Android API Level 24